import React from 'react';
import {Col, Panel, Row} from 'react-bootstrap';

import Vendor from './Vendor';

export default class Quote extends React.Component {
    static propTypes: {
        quote: React.PropTypes.object.isRequired
    };

    render() {
        return (
            this.props.quote !== null ?
                this.props.quote.internalType == 'purchase' ?
                    <PurchaseQuote quote={this.props.quote}/>
                : this.props.quote.internalType == 'repair' ?
                    <RepairQuote quote={this.props.quote}/>
                :
                    <NoQuote/>
            : null
        );
    }
}

class PurchaseQuote extends React.Component {
    propTypes: {
        quote: React.PropTypes.object.isRequired
    };

    render() {
        return (
            <Panel header="Quote">
                <Row>
                    {typeof(this.props.quote.vendor) !== 'undefined' ?
                        <Col md={4}>
                            <Vendor vendor={this.props.quote.vendor}/>
                        </Col>
                    :
                        null
                    }
                    <Col md={3}>
                        <p><strong>Alternate:</strong> {this.props.quote.alternate}</p>
                        <p><strong>Price:</strong> {this.props.quote.price}</p>
                        <p><strong>Condition:</strong> {this.props.quote.condition}</p>
                    </Col>
                    <Col md={3}>
                        <p><strong>Trace:</strong> {this.props.quote.trace}</p>
                        <p><strong>Qty:</strong> {this.props.quote.qty}</p>
                        <p><strong>U/M:</strong> {this.props.quote.um}</p>
                    </Col>
                    <Col md={2}>
                        <p><strong>Tag Ingo:</strong> {this.props.quote.tagInfo}</p>
                        <p><strong>Lead Time:</strong> {this.props.quote.leadTime}</p>
                        <p><strong>Comments:</strong> {this.props.quote.comments}</p>
                    </Col>
                </Row>
            </Panel>
        );
    }
}

class RepairQuote extends React.Component {
    static propTypes: {
        quote: React.PropTypes.object.isRequired
    };

    render() {
        return (
            <Panel header="Quote">
                <Row>
                    {typeof(this.props.quote.vendor) !== 'undefined' ?
                        <Col md={4}>
                            <Vendor vendor={this.props.quote.vendor}/>
                        </Col>
                        :
                        null
                    }
                    <Col md={3}>
                        <p><strong>Alternate:</strong> {this.props.quote.alternate}</p>
                        <p><strong>Comments:</strong> {this.props.quote.comments}</p>
                    </Col>
                    <Col md={3}>
                        <p><strong>Repair:</strong> {this.props.quote.repairPrice}</p>
                        <p><strong>Overhaul:</strong> {this.props.quote.overhaulPrice}</p>
                        <p><strong>Inspected:</strong> {this.props.quote.inspectedPrice}</p>
                    </Col>
                    <Col md={2}>
                        <p><strong>T.A.T.:</strong> {this.props.quote.repairTat}</p>
                        <p><strong>T.A.T.:</strong> {this.props.quote.overhaulTat}</p>
                        <p><strong>T.A.T.:</strong> {this.props.quote.inspectedTat}</p>
                    </Col>
                </Row>
            </Panel>
        );
    }
}

class NoQuote extends React.Component {
    render() {
        return (
            <Panel header="No Quote">
            </Panel>
        );
    }
}