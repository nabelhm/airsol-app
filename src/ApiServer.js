export default class ApiServer  {
    constructor() {
        this.debug = false;

        this.base = 'http://airlinessolutions.cubalider.com';
        this.caller = require('superagent');
        this.storer = require('sessionstorage');
        this.decoder = require('base-64');
    }

    get(url) {
        this.call = !this.debug ?
            this.caller.get(this.base + url) :
            this.caller.get(this.base + url + '?XDEBUG_SESSION_START=phpstorm');

        return this;
    }

    post(url) {
        this.call = !this.debug ?
            this.caller.post(this.base + url) :
            this.caller.post(this.base + url + '?XDEBUG_SESSION_START=phpstorm');

        return this;
    }

    auth(email, password) {
        if (typeof(email) !== 'undefined' && typeof(password) !== 'undefined') {
            this.call.auth(
                email,
                password
            );
        } else {
            this.call.auth(
                this.storer.getItem('email'),
                this.decoder.decode(this.storer.getItem('password'))
            );
        }

        return this;
    }

    send(data) {
        this.call.send(data);

        return this;
    }

    field(key, value) {
        this.call.field(key, value);

        return this;
    }

    attach(key, value) {
        this.call.attach(key, value);

        return this;
    }

    end(callback) {
        this.call.end(callback);
    }
}
