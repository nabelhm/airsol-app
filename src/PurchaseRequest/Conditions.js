import React from 'react';
import {ButtonToolbar, Grid, Input, Modal, Panel, Table, Well} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import BusyIndicator from '../BusyIndicator';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';
import CreateCondition from './CreateCondition';
import DeleteCondition from './DeleteCondition';
import UpdateCondition from './UpdateCondition';

export default class Conditions extends React.Component {
    static propTypes: {
        onLogin: React.PropTypes.func.isRequired,
        onCreate: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            conditions: null
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
    }

    render() {
        var conditions = [];

        if (this.state.conditions !== null) {
            conditions = this.state.conditions.map(function (condition) {
                return (
                    <tr key={condition.id}>
                        <td>{condition.name}</td>
                        <td>
                            <ButtonToolbar>
                                <UpdateCondition condition={condition} onUpdate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                                <DeleteCondition condition={condition} onDelete={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                            </ButtonToolbar>
                        </td>
                    </tr>
                );
            }, this);
        }

        return (
            <Grid fluid>
                <Well>
                    <ButtonToolbar>
                        <CreateCondition onCreate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                    </ButtonToolbar>
                </Well>
                <Panel header="List of conditions">
                    {this.state.conditions === null ?
                        <BusyIndicator/> :
                        this.state.conditions.length == 0 ?
                            'No conditions' :
                            <Table striped condensed hover>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {conditions}
                                </tbody>
                            </Table>
                    }
                </Panel>
            </Grid>
        )
    }
    
    componentDidMount() {
        this.collectConditions();
    }

    collectConditions() {
        this.apiServer.get('/purchase-request/collect-conditions').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');
            }

            this.setState({
                conditions: res.body
            });
        }.bind(this));
    }

    _handleChange(conditions) {
        this.setState({
            conditions: conditions
        });
    }

    _handleUnauthorized() {
        this.transitionTo('login');
    }
}
