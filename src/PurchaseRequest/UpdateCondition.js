import React from 'react';
import {Grid, Input, ButtonToolbar, Modal} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';

export default class UpdateCondition extends React.Component {
    static propTypes: {
        condition: React.PropTypes.object.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            overlay: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleUpdate = this._handleUpdate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <div>
                <Button
                    icon="edit"
                    textBefore="Edit"
                    onClick={this._handleClick}
                />
                <Modal show={this.state.overlay} backdrop={false} enforceFocus={false} onHide={this._handleCancel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Update condition</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form condition={this.props.condition} onUpdate={this._handleUpdate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

    _handleClick(finish) {
        this.setState({
            overlay: true
        });

        finish();
    }

    _handleUpdate(conditions) {
        this.setState({
            overlay: false
        });

        this.props.onUpdate(conditions);
    }

    _handleCancel() {
        this.setState({
            overlay: false
        });
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class Form extends React.Component {
    static propTypes: {
        condition: React.PropTypes.object.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                name: this.props.condition.name
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                <Input
                    ref="name"
                    value={this.state.request.name}
                    type="text"
                    label="Name"
                    help="Name of condition."
                    labelClassName="col-xs-2" wrapperClassName="col-xs-10"
                    onChange={this._handleChange}
                    autoFocus/>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <ButtonToolbar>
                            <Button
                                type="submit"
                                icon="ok-sign"
                                textBefore="Update"
                                textAfter="Updating..."
                                bsStyle="primary"
                                onClick={this._handleSubmit}
                            />
                            <Button
                                icon="remove-sign"
                                textBefore="Cancel"
                                onClick={this._handleCancel}
                            />
                        </ButtonToolbar>
                    </div>
                </div>
            </form>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                name: this.refs.name.getValue()
            }
        });
    }

    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/purchase-request/update-condition/' + this.props.condition.id)
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onUpdate(res.body);
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel(finish) {
        finish();

        this.props.onCancel();
    }
}