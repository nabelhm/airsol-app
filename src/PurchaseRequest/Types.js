import React from 'react';
import {ButtonToolbar, Grid, Input, Modal, Panel, Table, Well} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import BusyIndicator from '../BusyIndicator';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';
import CreateType from './CreateType';
import DeleteType from './DeleteType';
import UpdateType from './UpdateType';

export default class Types extends React.Component {
    static propTypes: {
        onLogin: React.PropTypes.func.isRequired,
        onCreate: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            types: null
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
    }

    render() {
        var types = [];

        if (this.state.types !== null) {
            types = this.state.types.map(function (type) {
                return (
                    <tr key={type.id}>
                        <td>{type.name}</td>
                        <td>
                            <ButtonToolbar>
                                <UpdateType type={type} onUpdate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                                <DeleteType type={type} onDelete={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                            </ButtonToolbar>
                        </td>
                    </tr>
                );
            }, this);
        }

        return (
            <Grid fluid>
                <Well>
                    <ButtonToolbar>
                        <CreateType onCreate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                    </ButtonToolbar>
                </Well>
                <Panel header="List of types">
                    {this.state.types === null ?
                        <BusyIndicator/> :
                        this.state.types.length == 0 ?
                            'No types' :
                            <Table striped condensed hover>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {types}
                                </tbody>
                            </Table>
                    }
                </Panel>
            </Grid>
        )
    }
    
    componentDidMount() {
        this.collectTypes();
    }

    collectTypes() {
        this.apiServer.get('/purchase-request/collect-types').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');
            }

            this.setState({
                types: res.body
            });
        }.bind(this));
    }

    _handleChange(types) {
        this.setState({
            types: types
        });
    }

    _handleUnauthorized() {
        this.transitionTo('login');
    }
}
