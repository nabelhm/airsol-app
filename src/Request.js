import React from 'react';
import ReactDOM from 'react-dom';
import {DropdownList, SelectList} from 'react-widgets';
import {Accordion, ButtonToolbar, Col, Glyphicon, Grid, Input, Modal, Panel, Row, Well} from 'react-bootstrap';
import Moment from 'moment'; Moment.locale('en');
import {LinkContainer} from 'react-router-bootstrap';

import ApiServer from './ApiServer';
import BusyIndicator from './BusyIndicator';
import Button from './Button';
import ResponseAlert from './ResponseAlert';
import Vendor from './Vendor';

export default class Request extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        priorities: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Grid fluid>
                {this.props.request.internalType == 'purchase' ?
                    <PurchaseRequest request={this.props.request} priorities={this.props.priorities} types={this.props.types} conditions={this.props.conditions}/> :
                    <RepairRequest request={this.props.request} priorities={this.props.priorities}/>
                }
            </Grid>
        );
    }
}

class PurchaseRequest extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        priorities: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col md={4}>
                    <p><Glyphicon glyph="shopping-cart"/> <strong>Purchase</strong></p>
                    <p><strong>Part Number:</strong> {this.props.request.partNumber}</p>
                    <p><strong>Description:</strong> {this.props.request.description}</p>
                    <p><strong>Priority:</strong> <Priority id={this.props.request.priority} priorities={this.props.priorities}/></p>
                </Col>
                <Col md={4}>
                    <p><strong>Type:</strong> <Type id={this.props.request.type} types={this.props.types}/></p>
                    <p><strong>Comments:</strong> {this.props.request.comments}</p>
                    <p><strong>Alternate:</strong> {this.props.request.alternate}</p>
                    <strong>Conditions:</strong> <Conditions ids={this.props.request.conditions} conditions={this.props.conditions}/>
                </Col>
                <Col md={4}>
                    <p><strong>QTY:</strong> {this.props.request.qty}</p>
                    <p><strong>UM:</strong> {this.props.request.um}</p>
                    <p><strong>Aircraft Type:</strong> {this.props.request.aircraftType}</p>
                    <strong>Created: </strong><abbr title={Moment.unix(this.props.request.created).format('LLLL')}>{Moment.unix(this.props.request.created).fromNow()}</abbr>
                </Col>
            </Row>
        );
    }
}

class RepairRequest extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        priorities: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col md={4}>
                    <p><Glyphicon glyph="wrench"/> <strong>Repair</strong></p>
                    <p><strong>Part Number:</strong> {this.props.request.partNumber}</p>
                    <p><strong>Description:</strong> {this.props.request.description}</p>
                    <p><strong>Priority:</strong> <Priority id={this.props.request.priority} priorities={this.props.priorities}/></p>
                </Col>
                <Col md={4}>
                    <p><strong>Comments:</strong> {this.props.request.comments}</p>
                    <p><strong>Alternate:</strong> {this.props.request.alternate}</p>
                    <p><strong>Inspected:</strong> {this.props.request.inspected ? "Yes" : "No"}</p>
                    <p><strong>Repair:</strong> {this.props.request.repair ? "Yes" : "No"}</p>
                    <p><strong>Overhaul:</strong> {this.props.request.overhaul ? "Yes" : "No"}</p>
                </Col>
                <Col md={4}>
                    <p><strong>QTY:</strong> {this.props.request.qty}</p>
                    <p><strong>Aircraft Type:</strong> {this.props.request.aircraftType}</p>
                    <strong>Created: </strong><abbr title={Moment.unix(this.props.request.created).format('LLLL')}>{Moment.unix(this.props.request.created).fromNow()}</abbr>
                </Col>
            </Row>
        );
    }
}

class Priority extends React.Component {
    static propTypes: {
        id: React.PropTypes.number.isRequired,
        priorities: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);

        this._resolve = this._resolve.bind(this);
    }

    render() {
        return (
            <span>
                {this._resolve()}
            </span>
        );
    }

    _resolve() {
        let priority = this.props.priorities.find(function(priority) {
            return this.props.id == priority.id;
        }.bind(this));

        return priority.name;
    }
}

class Type extends React.Component {
    static propTypes: {
        type: React.PropTypes.number.isRequired,
        types: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);

        this._resolve = this._resolve.bind(this);
    }

    render() {
        return (
            <span>
                {this._resolve()}
            </span>
        );
    }

    _resolve() {
        let type = this.props.types.find(function(type) {
            return this.props.id == type.id;
        }.bind(this));

        return type.name;
    }
}

class Conditions extends React.Component {
    static propTypes: {
        ids: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);

        this._resolve = this._resolve.bind(this);
    }

    render() {
        return (
            this._resolve()
        );
    }

    _resolve() {
        let names = [];
        this.props.ids.map(function(id) {
            let condition = this.props.conditions.find(function(condition) {
                return id == condition.id;
            }.bind(this));

            names.push(
                <li key={condition.id}>{condition.name}</li>
            );
        }.bind(this));

        return <ul>{names}</ul>;
    }
}

class PurchaseQuotes extends React.Component {
    static propTypes: {
        quotes: React.PropTypes.array.isRequired,
        style: React.PropTypes.object
        };

    render() {
        var quotes = this.props.quotes.map(function (quote) {
            return (
                <PurchaseQuote key={quote.id} quote={quote}/>
            );
        });

        return (
            <div style={this.props.style}>
                {quotes}
            </div>
        );
    }
}

class PurchaseQuote extends React.Component {
    propTypes: {
        quote: React.PropTypes.object.isRequired
        };

    render() {
        return (
            <Panel header="Quote">
                <Vendor vendor={this.props.quote.vendor}/>
                <Row>
                    <Col md={3}>
                        <p><strong>Alternate:</strong> {this.props.quote.alternate}</p>
                        <p><strong>Condition:</strong> {this.props.quote.condition}</p>
                        <p><strong>Qty:</strong> {this.props.quote.qty}</p>
                    </Col>
                    <Col md={4}>
                        <p><strong>Tag Ingo:</strong> {this.props.quote.tagInfo}</p>
                        <p><strong>Price:</strong> {this.props.quote.price}</p>
                        <p><strong>Trace:</strong> {this.props.quote.trace}</p>
                    </Col>
                    <Col md={4}>
                        <p><strong>Comments:</strong> {this.props.quote.comments}</p>
                        <p><strong>U/M:</strong> {this.props.quote.um}</p>
                        <p><strong>Lead Time:</strong> {this.props.quote.leadTime}</p>
                    </Col>
                </Row>
            </Panel>
        );
    }
}

class RepairQuotes extends React.Component {
    static propTypes: {
        quotes: React.PropTypes.array.isRequired,
        style: React.PropTypes.object
        };

    render() {
        var quotes = this.props.quotes.map(function (quote) {
            return (
                <RepairQuote key={quote.id} quote={quote}/>
            );
        });

        return (
            <div style={this.props.style}>
                {quotes}
            </div>
        );
    }
}

class RepairQuote extends React.Component {
    static propTypes: {
        quote: React.PropTypes.object.isRequired
        };

    render() {
        return (
            <Panel header="Quote">
                <Vendor vendor={this.props.quote.vendor}/>
                <Row>
                    <Col md={4}>
                        <p><strong>Alternate:</strong> {this.props.quote.alternate}</p>
                        <p><strong>Inspected:</strong> {this.props.quote.inspectedPrice}</p>
                        <p><strong>T.A.T.:</strong> {this.props.quote.inspectedTat}</p>
                    </Col>
                    <Col md={4}>
                        <p><strong>Repair:</strong> {this.props.quote.repairPrice}</p>
                        <p><strong>T.A.T.:</strong> {this.props.quote.repairTat}</p>
                        <p><strong>Overhaul:</strong> {this.props.quote.overhaulPrice}</p>
                        <p><strong>T.A.T.:</strong> {this.props.quote.overhaulTat}</p>
                    </Col>
                    <Col md={4}>
                        <p><strong>Comments:</strong> {this.props.quote.comments}</p>
                    </Col>
                </Row>
            </Panel>
        );
    }
}