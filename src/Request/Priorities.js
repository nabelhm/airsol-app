import React from 'react';
import {ButtonToolbar, Grid, Input, Modal, Panel, Table, Well} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import BusyIndicator from '../BusyIndicator';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';
import CreatePriority from './CreatePriority';
import DeletePriority from './DeletePriority';
import UpdatePriority from './UpdatePriority';

export default class Priorities extends React.Component {
    static propTypes: {
        onLogin: React.PropTypes.func.isRequired,
        onCreate: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            priorities: null
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
    }

    render() {
        var priorities = [];

        if (this.state.priorities !== null) {
            priorities = this.state.priorities.map(function (priority) {
                return (
                    <tr key={priority.id}>
                        <td>{priority.name}</td>
                        <td>
                            <ButtonToolbar>
                                <UpdatePriority priority={priority} onUpdate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                                <DeletePriority priority={priority} onDelete={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                            </ButtonToolbar>
                        </td>
                    </tr>
                );
            }, this);
        }

        return (
            <Grid fluid>
                <Well>
                    <ButtonToolbar>
                        <CreatePriority onCreate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                    </ButtonToolbar>
                </Well>
                <Panel header="List of priorities">
                    {this.state.priorities === null ?
                        <BusyIndicator/> :
                        this.state.priorities.length == 0 ?
                            'No priorities' :
                            <Table striped condensed hover>
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {priorities}
                                </tbody>
                            </Table>
                    }
                </Panel>
            </Grid>
        )
    }
    
    componentDidMount() {
        this.collectPriorities();
    }

    collectPriorities() {
        this.apiServer.get('/request/collect-priorities').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');
            }

            this.setState({
                priorities: res.body
            });
        }.bind(this));
    }

    _handleChange(priorities) {
        this.setState({
            priorities: priorities
        });
    }

    _handleUnauthorized() {
        this.transitionTo('login');
    }
}
