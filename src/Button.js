import React from 'react';
import {Button as BootstrapButton, Glyphicon} from 'react-bootstrap';

export default class Button extends React.Component {
    static propTypes: {
        icon: React.PropTypes.string.isRequired,
        textBefore: React.PropTypes.string.isRequired,
        textAfter: React.PropTypes.string,
        onClick: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            doing: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleFinish = this._handleFinish.bind(this);
    }
    
    _handleClick() {
        this.setState({
            doing: true
        });

        this.props.onClick(this._handleFinish);
    }
    
    _handleFinish() {
        this.setState({
            doing: false
        });
    }
    
    render() {
        return (
            <BootstrapButton {...this.props} onClick={this._handleClick} disabled={this.props.textAfter && this.state.doing}>
                <Glyphicon glyph={this.props.icon}/> {!this.props.textAfter || !this.state.doing ? this.props.textBefore : this.props.textAfter}
            </BootstrapButton>
        );
    }
}
