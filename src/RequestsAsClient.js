import React from 'react';
import ReactDOM from 'react-dom';
import {DropdownList, SelectList} from 'react-widgets';
import {Accordion, ButtonToolbar, Col, Glyphicon, Grid, Input, Modal, Panel, Row, Well} from 'react-bootstrap';
import Moment from 'moment'; Moment.locale('en');
import {LinkContainer} from 'react-router-bootstrap';

import ApiServer from './ApiServer';
import BusyIndicator from './BusyIndicator';
import Button from './Button';
import ResponseAlert from './ResponseAlert';
import Request from './Request';
import Quote from './Quote';

export default class RequestsAsClient extends React.Component {
    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();
        
        this.state = {
            priorities: null,
            types: null,
            conditions: null,
            requests: null
        };
        
        this._collectPriorities = this._collectPriorities.bind(this);
        this._collectTypes = this._collectTypes.bind(this);
        this._collectConditions = this._collectConditions.bind(this);
        this._collectRequests = this._collectRequests.bind(this);
        this._handleChange = this._handleChange.bind(this);
    }

    render() {
        var requests = [];

        if (
            this.state.priorities !== null
            && this.state.types !== null
            && this.state.conditions !== null
            && this.state.requests !== null
        ) {
            requests = this.state.requests.map(function (request) {
                var quotes = request.quotes.map(function (quote) {
                    return (
                        <Quote quote={quote} key={quote.id}/>
                    );
                });

                return (
                    <Panel header={"Request # " + request.id} eventKey={request.id} key={request.id}>
                        <Request request={request} priorities={this.state.priorities} types={this.state.types} conditions={this.state.conditions}/>
                        <Grid fluid>
                            <Row>
                                <Col md={4}>
                                    <p><strong>Quotes: </strong> {request.quotes.length} {request.length != 1 ? "quotes" : "quote"}</p>
                                </Col>
                            </Row>
                        </Grid>
                        {quotes}
                    </Panel>
                )
            }, this);
        }

        return (
            <Grid fluid>
                <Well><ButtonToolbar>
                    {
                        this.state.priorities === null
                        || this.state.types === null
                        || this.state.conditions === null
                    ?
                        <BusyIndicator/> :
                        <CreateRequestButton
                            priorities={this.state.priorities}
                            types={this.state.types}
                            conditions={this.state.conditions}
                            onCreate={this._handleChange}
                            onUnauthorized={this._handleUnauthorized}
                        />
                    }
                </ButtonToolbar></Well>
                <Panel header="List of requests">
                    { this.state.requests === null ?
                        <BusyIndicator/> :
                        this.state.requests.length == 0 ?
                            'No requests' :
                            <Accordion>{requests}</Accordion>
                    }
                </Panel>
            </Grid>
        );
    }
    
    componentDidMount() {
        this._collectPriorities();
        this._collectTypes();
        this._collectConditions();
        this._collectRequests();
    }
    
    _collectPriorities() {
        this.apiServer.get('/request/collect-priorities').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                priorities: res.body
            });
        }.bind(this));
    }
    
    _collectTypes() {
        this.apiServer.get('/purchase-request/collect-types').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                types: res.body
            });
        }.bind(this));
    }

    _collectConditions() {
        this.apiServer.get('/purchase-request/collect-conditions').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                conditions: res.body
            });
        }.bind(this));
    }
    
    _collectRequests() {
        this.apiServer.get('/collect-requests-as-client').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                requests: res.body
            });
        }.bind(this));
    }

    _handleChange(requests) {
        this.setState({
            requests: requests
        });
    }
}

class CreateRequestButton extends React.Component {
    static propTypes: {
        priorities: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired,
        onCreate: React.PropTypes.func,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            overlay: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleCreate = this._handleCreate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <div>
                <Button
                    icon="plus"
                    textBefore="Create request for quote"
                    onClick={this._handleClick}
                />
                <Modal show={this.state.overlay} backdrop={false} enforceFocus={false} onHide={this._handleCancel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Create request for quote</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <CreateRequestButtons priorities={this.props.priorities} types={this.props.types} conditions={this.props.conditions} onCreate={this._handleCreate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

    _handleClick() {
        this.setState({
            overlay: true
        });
    }

    _handleCreate(requests) {
        this.setState({
            overlay: false
        });

        this.props.onCreate(requests);
    }

    _handleCancel() {
        this.setState({
            overlay: false
        });
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class CreateRequestButtons extends React.Component {
    static propTypes: {
        priorities: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired,
        onCreate: React.PropTypes.func,
        onCancel: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    } ;

    constructor(props) {
        super(props);

        this.state = {
            view: 'buttons'
        };

        this._handleCreate = this._handleCreate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
        this._handlePurchase = this._handlePurchase.bind(this);
        this._handleRepair = this._handleRepair.bind(this);
    }

    render() {
        return (
            this.state.view == 'buttons'
                ?
                <div className="text-center">
                    <ButtonToolbar style={{"display": "inline-block"}}>
                        <Button
                            icon="shopping-cart"
                            textBefore="Purchase"
                            bsSize="large"
                            onClick={this._handlePurchase}
                        />
                        <Button
                            icon="wrench"
                            textBefore="Repair"
                            bsSize="large"
                            onClick={this._handleRepair}
                        />
                    </ButtonToolbar>
                </div>
                :
                this.state.view == 'purchase'
                    ?
                    <CreatePurchaseRequestForm priorities={this.props.priorities} types={this.props.types} conditions={this.props.conditions} onCreate={this._handleCreate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    :
                    <CreateRepairRequestForm priorities={this.props.priorities} onCreate={this._handleCreate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
        );
    }

    _handleCreate(requests) {
        this.props.onCreate(requests);
    }

    _handleCancel() {
        this.props.onCancel();
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }

    _handlePurchase() {
        this.setState({
            view: 'purchase'
        });
    }

    _handleRepair() {
        this.setState({
            view: 'repair'
        });
    }
}

class CreatePurchaseRequestForm extends React.Component {
    static propTypes: {
        priorities: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        conditions: React.PropTypes.array.isRequired,
        onCreate: React.PropTypes.func,
        onCancel: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                partNumber: null,
                description: null,
                priority: null,
                type: null,
                comments: null,
                alternate: null,
                conditions: null,
                qty: null,
                um: null,
                aircraftType: null
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
        this._handleChangePriority = this._handleChangePriority.bind(this);
        this._handleChangeType = this._handleChangeType.bind(this);
        this._handleChangeConditions = this._handleChangeConditions.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                <h1><Glyphicon glyph="shopping-cart" /> Purchase</h1>
                <Row>
                    <Col md={6}>
                        <Input
                            ref="partNumber"
                            value={this.state.request.partNumber}
                            type="text"
                            label="Part Number"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                            autoFocus
                        />
                        <Input
                            ref="description"
                            value={this.state.request.description}
                            type="textarea"
                            rows="4"
                            label="Description"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <div className="form-group">
                            <label className="control-label col-xs-4">Priority</label>
                            <div className="col-xs-8">
                                <DropdownList
                                    ref='priority'
                                    data={this.props.priorities}
                                    valueField='id'
                                    textField='name'
                                    onChange={this._handleChangePriority}/>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label col-xs-4">Type</label>
                            <div className="col-xs-8">
                                <SelectList
                                    ref='type'
                                    data={this.props.types}
                                    valueField='id'
                                    textField='name'
                                    onChange={this._handleChangeType}
                                />
                            </div>
                        </div>
                        <Input
                            ref="comments"
                            value={this.state.request.comments}
                            type="textarea"
                            rows="4"
                            label="Comments"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                    </Col>
                    <Col md={6}>
                        <Input
                            ref="alternate"
                            value={this.state.request.alternate}
                            type="text"
                            label="Alternate"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <div className="form-group">
                            <label className="control-label col-xs-4">Conditions</label>
                            <div className="col-xs-8">
                                <SelectList
                                    ref='conditions'
                                    data={this.props.conditions}
                                    valueField='id'
                                    textField='name'
                                    multiple
                                    onChange={this._handleChangeConditions}/>
                            </div>
                        </div>
                        <Input
                            ref="qty"
                            value={this.state.request.qty}
                            type="text"
                            label="QTY"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="um"
                            value={this.state.request.um}
                            type="text"
                            label="UM"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="aircraftType"
                            value={this.state.request.aircraftType}
                            type="text"
                            label="Aircraft Type"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                    </Col>
                </Row>
                <div className="form-group text-center">
                    <ButtonToolbar style={{"display": "inline-block"}}>
                        <Button
                            type="submit"
                            icon="ok-sign"
                            textBefore="Create"
                            textAfter="Creating..."
                            bsStyle="primary"
                            onClick={this._handleSubmit}
                        />
                        <Button
                            icon="remove-sign"
                            textBefore="Cancel"
                            bsStyle="primary"
                            onClick={this._handleCancel}
                        />
                    </ButtonToolbar>
                </div>
            </form>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                ...this.state.request,
                partNumber: this.refs.partNumber.getValue(),
                description: this.refs.description.getValue(),
                comments: this.refs.comments.getValue(),
                alternate: this.refs.alternate.getValue(),
                qty: this.refs.qty.getValue(),
                um: this.refs.um.getValue(),
                aircraftType: this.refs.aircraftType.getValue()
            }
        });
    }

    _handleChangePriority(priority) {
        this.setState({
            request: {
                ...this.state.request,
                priority: priority.id
            }
        });
    }

    _handleChangeType(type) {
        this.setState({
            request: {
                ...this.state.request,
                type: type.id
            }
        });
    }

    _handleChangeConditions(conditions) {
        Object.keys(conditions).map(function (index) {
            conditions[index] = conditions[index].id;
        });

        this.setState({
            request: {
                ...this.state.request,
                conditions: conditions
            }
        });
    }
    
    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/create-purchase-request')
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onCreate(
                        res.body
                    );
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class CreateRepairRequestForm extends React.Component {
    static propTypes: {
        priorities: React.PropTypes.array.isRequired,
        onCreate: React.PropTypes.func,
        onCancel: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                partNumber: null,
                description: null,
                priority: null,
                inspected: null,
                repair: null,
                overhaul: null,
                comments: null,
                alternate: null,
                qty: null,
                aircraftType: null
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
        this._handleChangePriority = this._handleChangePriority.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                <h1><Glyphicon glyph="wrench" /> Repair</h1>
                <Row>
                    <Col md={6}>
                        <Input
                            ref="partNumber"
                            value={this.state.request.partNumber}
                            type="text"
                            label="Part Number"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                            autoFocus
                        />
                        <Input
                            ref="description"
                            value={this.state.request.description}
                            type="textarea"
                            rows="4"
                            label="Description"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <div className="form-group">
                            <label className="control-label col-xs-4">Priority</label>
                            <div className="col-xs-8">
                                <DropdownList
                                    ref='priority'
                                    data={this.props.priorities}
                                    valueField='id'
                                    textField='name'
                                    onChange={this._handleChangePriority}/>
                            </div>
                        </div>
                        <Input
                            ref="inspected"
                            id="inspected"
                            checked={this.state.request.inspected}
                            type="checkbox"
                            label="Inspected"
                            wrapperClassName="col-xs-offset-4 col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="repair"
                            id="repair"
                            checked={this.state.request.repair}
                            type="checkbox"
                            label="Repair"
                            wrapperClassName="col-xs-offset-4 col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="overhaul"
                            id="overhaul"
                            checked={this.state.request.overhaul}
                            type="checkbox"
                            label="Overhaul"
                            wrapperClassName="col-xs-offset-4 col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="comments"
                            value={this.state.request.comments}
                            type="textarea"
                            rows="4"
                            label="Comments"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                    </Col>
                    <Col md={6}>
                        <Input
                            ref="alternate"
                            value={this.state.request.alternate}
                            type="text"
                            label="Alternate"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="qty"
                            value={this.state.request.qty}
                            type="text"
                            label="QTY"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                        <Input
                            ref="aircraftType"
                            value={this.state.request.aircraftType}
                            type="text"
                            label="Aircraft Type"
                            labelClassName="col-xs-4"
                            wrapperClassName="col-xs-8"
                            onChange={this._handleChange}
                        />
                    </Col>
                </Row>
                <div className="form-group text-center">
                    <ButtonToolbar style={{"display": "inline-block"}}>
                        <Button
                            type="submit"
                            icon="ok-sign"
                            textBefore="Create"
                            textAfter="Creating..."
                            bsStyle="primary"
                            onClick={this._handleSubmit}
                        />
                        <Button
                            icon="remove-sign"
                            textBefore="Cancel"
                            onClick={this._handleCancel}
                        />
                    </ButtonToolbar>
                </div>
            </form>
        );
    }
    
    _handleChange() {
        this.setState({
            request: {
                ...this.state.request,
                partNumber: this.refs.partNumber.getValue(),
                description: this.refs.description.getValue(),
                inspected: this.refs.inspected.getChecked(),
                repair: this.refs.repair.getChecked(),
                overhaul: this.refs.overhaul.getChecked(),
                comments: this.refs.comments.getValue(),
                alternate: this.refs.alternate.getValue(),
                qty: this.refs.qty.getValue(),
                aircraftType: this.refs.aircraftType.getValue()
            }
        });
    }
    
    _handleChangePriority(priority) {
        this.setState({
            request: {
                partNumber: this.state.request.partNumber,
                description: this.state.request.description,
                priority: priority.id,
                inspected: this.state.request.inspected,
                repair: this.state.request.repair,
                overhaul: this.state.request.overhaul,
                comments: this.state.request.comments,
                alternate: this.state.request.alternate,
                qty: this.state.request.qty,
                aircraftType: this.state.request.aircraftType
            }
        });
    }
    
    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/create-repair-request')
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onCreate(
                        res.body
                    );
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }
    
    _handleCancel() {
        this.props.onCancel();
    }
    
    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}
