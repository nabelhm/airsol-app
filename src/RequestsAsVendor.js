import React from 'react';
import ReactDOM from 'react-dom';
import {DropdownList, SelectList} from 'react-widgets';
import {Accordion, ButtonToolbar, Col, Glyphicon, Grid, Input, Modal, Panel, Row, Well} from 'react-bootstrap';
import Moment from 'moment'; Moment.locale('en');
import {LinkContainer} from 'react-router-bootstrap';

import ApiServer from './ApiServer';
import BusyIndicator from './BusyIndicator';
import Button from './Button';
import ResponseAlert from './ResponseAlert';
import Request from './Request';
import Quote from './Quote';

export default class RequestsAsVendor extends React.Component {
    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();
        this.storer = require('sessionstorage');
        this.encoder = require('base-64');

        this.state = {
            priorities: null,
            types: null,
            conditions: null,
            requests: null,
            action: null
        };

        this._collectPriorities = this._collectPriorities.bind(this);
        this._collectTypes = this._collectTypes.bind(this);
        this._collectConditions = this._collectConditions.bind(this);
        this._collectRequests = this._collectRequests.bind(this);
        this._handleQuote = this._handleQuote.bind(this);
        this._handleNoQuote = this._handleNoQuote.bind(this);
        this._handleCreate = this._handleCreate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        var requests = [];

        if (
            this.state.priorities !== null
            && this.state.types !== null
            && this.state.conditions !== null
            && this.state.requests !== null
        ) {
            requests = this.state.requests.map(function (request) {
                return (
                    <Panel header={"Request # " + request.id} eventKey={request.id} key={request.id}>
                        <Request request={request} priorities={this.state.priorities} types={this.state.types} conditions={this.state.conditions}/>

                        <div style={{marginTop: "10px"}}>
                            <Quote quote={request.response}/>
                        </div>

                        <div style={{marginTop: "10px"}}>
                            {request.response === null ?
                                this.state.action == 'quote' ?
                                    request.internalType == 'purchase' ?
                                        <CreatePurchaseQuote request={request} onCreate={this._handleCreate} onCancel={this._handleCancel}/>
                                    :
                                        <CreateRepairQuote request={request} onCreate={this._handleCreate} onCancel={this._handleCancel}/>
                                    :
                                        this.state.action == 'no-quote' ?
                                            <CreateNoQuote request={request} onCreate={this._handleCreate} onCancel={this._handleCancel}/>
                                        :
                                            <ButtonToolbar>
                                                {request.response == null ?
                                                    <Button
                                                        icon="trash"
                                                        textBefore="No quote"
                                                        bsStyle="danger"
                                                        onClick={this._handleNoQuote}
                                                    />
                                                :
                                                    null
                                                }

                                                {request.response == null ?
                                                    <Button
                                                        icon="usd"
                                                        textBefore="Quote"
                                                        bsStyle="success"
                                                        onClick={this._handleQuote}
                                                    />
                                                :
                                                    null
                                                }
                                            </ButtonToolbar>
                                : null
                            }
                        </div>
                    </Panel>
                )
            }, this);
        }

        return (
            <Grid fluid>
                <Panel header="List of requests">
                    { this.state.requests === null ?
                        <BusyIndicator/> :
                        this.state.requests.length == 0 ?
                            'No requests'
                        :
                            <Accordion defaultExpanded={true} defaultActiveKey={this.props.params.request}>{requests}</Accordion>
                    }
                </Panel>
            </Grid>
        );
    }

    componentWillMount() {
        if (this.props.params.action == 'quote') {
            this.setState({
                action: 'quote'
            });
        } else if (this.props.params.action == 'no-quote') {
            this.setState({
                action: 'no-quote'
            });
        }

        if (typeof(this.props.params.token) !== 'undefined') {
            this.storer.setItem('email', this.props.params.token);
            this.storer.setItem('password', this.encoder.encode(this.props.params.token));
        }
    }

    componentDidMount() {
        this._collectPriorities();
        this._collectTypes();
        this._collectConditions();
        this._collectRequests();
    }

    _collectPriorities() {
        this.apiServer.get('/request/collect-priorities').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                priorities: res.body
            });
        }.bind(this));
    }

    _collectTypes() {
        this.apiServer.get('/purchase-request/collect-types').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                types: res.body
            });
        }.bind(this));
    }

    _collectConditions() {
        this.apiServer.get('/purchase-request/collect-conditions').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                conditions: res.body
            });
        }.bind(this));
    }

    _collectRequests() {
        this.apiServer.get('/collect-requests-as-vendor').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props._history.replaceState(null, '/login');
            }

            this.setState({
                requests: res.body
            });
        }.bind(this));
    }

    _handleQuote() {
        this.setState({
            'action': 'quote'
        });
    }

    _handleNoQuote() {
        this.setState({
            'action': 'no-quote'
        });
    }

    _handleCreate(requests) {
        this.setState({
            requests: requests,
            action: null
        });
    }

    _handleCancel() {
        this.setState({
            'action': null
        });

        //this.props.history.replaceState(null, `requests-as-vendor/${this.props.params.id}`);
    }
}

class CreatePurchaseQuote extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        onCreate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                request: this.props.request.id,
                alternate: null,
                condition: null,
                qty: null,
                tagInfo: null,
                price: null,
                trace: null,
                um: null,
                leadTime: null,
                comments: null
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <Panel header="Quote">
                <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                    <Input
                        ref="request"
                        value={this.props.request.id}
                        type="hidden"
                    />
                    <Row>
                        <Col md={4}>
                            <Input
                                ref="alternate"
                                value={this.state.request.alternate}
                                type="text"
                                label="Alternate"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                                autoFocus
                            />
                            <Input
                                ref="price"
                                value={this.state.request.price}
                                type="text"
                                label="Price *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="condition"
                                value={this.state.request.condition}
                                type="text"
                                label="Condition *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                        <Col md={4}>
                            <Input
                                ref="trace"
                                value={this.state.request.trace}
                                type="text"
                                label="Trace"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="qty"
                                value={this.state.request.qty}
                                type="text"
                                label="Qty *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="um"
                                value={this.state.request.um}
                                type="text"
                                label="U/M"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                        <Col md={4}>
                            <Input
                                ref="tagInfo"
                                value={this.state.request.tagInfo}
                                type="text"
                                label="Tag Info"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="leadTime"
                                value={this.state.request.leadTime}
                                type="text"
                                label="Lead Time"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="comments"
                                value={this.state.request.comments}
                                type="textarea"
                                rows="4"
                                label="Comments"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                    </Row>
                    <ButtonToolbar>
                        <Button
                            type="submit"
                            icon="ok-sign"
                            textBefore="Submit"
                            textAfter="Submitting..."
                            bsStyle="primary"
                            onClick={this._handleSubmit}
                        />
                        <Button
                            icon="remove-sign"
                            textBefore="Cancel"
                            onClick={this._handleCancel}
                        />
                    </ButtonToolbar>
                </form>
            </Panel>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                request: this.props.request.id,
                alternate: this.refs.alternate.getValue(),
                condition: this.refs.condition.getValue(),
                qty: this.refs.qty.getValue(),
                tagInfo: this.refs.tagInfo.getValue(),
                price: this.refs.price.getValue(),
                trace: this.refs.trace.getValue(),
                um: this.refs.um.getValue(),
                leadTime: this.refs.leadTime.getValue(),
                comments: this.refs.comments.getValue()
            }
        });
    }

    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/create-purchase-quote')
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onCreate(
                        res.body
                    );
                } else {
                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }
}

class CreateRepairQuote extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        onCreate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                request: this.props.request.id,
                alternate: null,
                inspectedPrice: null,
                inspectedTat: null,
                repairPrice: null,
                repairTat: null,
                overhaulPrice: null,
                overhaulTat: null,
                comments: null
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <Panel header="Quote">
                <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                    <Input
                        ref="request"
                        value={this.props.request.id}
                        type="hidden"
                    />
                    <Row>
                        <Col md={4}>
                            <Input
                                ref="alternate"
                                value={this.state.request.alternate}
                                type="text"
                                label="Alternate"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                                autoFocus
                            />
                            <Input
                                ref="comments"
                                value={this.state.request.comments}
                                type="textarea"
                                rows="2"
                                label="Comments"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                        <Col md={4}>
                            <Input
                                ref="repairPrice"
                                value={this.state.request.repairPrice}
                                type="text"
                                label="Repair *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="overhaulPrice"
                                value={this.state.request.overhaulPrice}
                                type="text"
                                label="Overhaul *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="inspectedPrice"
                                value={this.state.request.inspectedPrice}
                                type="text"
                                label="Inspected *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                        <Col md={4}>
                            <Input
                                ref="repairTat"
                                value={this.state.request.repairTat}
                                type="text"
                                label="T.A.T *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="inspectedTat"
                                value={this.state.request.inspectedTat}
                                type="text"
                                label="T.A.T *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                            <Input
                                ref="overhaulTat"
                                value={this.state.request.overhaulTat}
                                type="text"
                                label="T.A.T *"
                                labelClassName="col-xs-4"
                                wrapperClassName="col-xs-8"
                                onChange={this._handleChange}
                            />
                        </Col>
                    </Row>
                    <ButtonToolbar>
                        <Button
                            type="submit"
                            icon="ok-sign"
                            textBefore="Submit"
                            textAfter="Submitting..."
                            bsStyle="primary"
                            onClick={this._handleSubmit}
                        />
                        <Button
                            icon="remove-sign"
                            textBefore="Cancel"
                            onClick={this._handleCancel}
                        />
                    </ButtonToolbar>
                </form>
            </Panel>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                request: this.props.request.id,
                alternate: this.refs.alternate.getValue(),
                inspectedPrice: this.refs.inspectedPrice.getValue(),
                inspectedTat: this.refs.inspectedTat.getValue(),
                repairPrice: this.refs.repairPrice.getValue(),
                repairTat: this.refs.repairTat.getValue(),
                overhaulPrice: this.refs.overhaulPrice.getValue(),
                overhaulTat: this.refs.overhaulTat.getValue(),
                comments: this.refs.comments.getValue()
            }
        });
    }

    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/create-repair-quote')
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onCreate(
                        res.body
                    );
                } else {
                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }
}

class CreateNoQuote extends React.Component {
    static propTypes: {
        request: React.PropTypes.object.isRequired,
        onCreate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                request: this.props.request.id
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <Panel header="No Quote">
                <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                    <Input
                        ref="request"
                        value={this.props.request.id}
                        type="hidden"
                    />
                    <ButtonToolbar>
                        <Button
                            type="submit"
                            icon="ok-sign"
                            textBefore="Submit"
                            textAfter="Submitting..."
                            bsStyle="primary"
                            onClick={this._handleSubmit}
                        />
                        <Button
                            icon="remove-sign"
                            textBefore="Cancel"
                            onClick={this._handleCancel}
                        />
                    </ButtonToolbar>
                </form>
            </Panel>
        );
    }

    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/create-no-quote')
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onCreate(
                        res.body
                    );
                } else {
                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }
}
