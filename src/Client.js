import React from 'react';
import {Col, Row} from 'react-bootstrap';

export default class Client extends React.Component {
    static propTypes: {
        client: React.PropTypes.object.isRequired
    };

    render() {
        return (
            <div>
                <Row>
                    <Col md={4}>
                        <p><strong>From:</strong></p>
                    </Col>
                </Row>
                <Row>
                    <Col md={1}>
                        <p><img src={this.props.client.logo} style={{'width': 80, 'height': 80}}/></p>
                    </Col>
                    <Col md={3}>
                        {this.props.client.company}<br />
                        {this.props.client.title == 'mr' ? 'Mr' : 'Ms'} {this.props.client.firstName}  {this.props.client.lastName}<br />
                        {this.props.client.address}, {this.props.client.zip}, {this.props.client.city}, {this.props.client.country}<br />
                        {this.props.client.phone}<br />
                        {this.props.client.email}
                    </Col>
                </Row>
            </div>
        );
    }
}
