import React from 'react';
import {Alert} from 'react-bootstrap';

export default class ResponseAlert extends React.Component {
    static propTypes: {
        status: React.PropTypes.number,
        message: React.PropTypes.string
    };

    resolveStyle(status) {
        if (status == 200) {
            return 'success';
        }

        return 'danger';
    }

    resolvePre(status) {
        if (status == 200) {
            return 'Ok! ';
        }

        return 'Error! ';
    }

    render() {
        return (
            <Alert bsStyle={this.resolveStyle(this.props.status)}>
                <p><strong>{this.resolvePre(this.props.status)}</strong> {this.props.message}</p>
            </Alert>
        );
    }
}
