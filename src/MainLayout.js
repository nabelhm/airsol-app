import React from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';

import ApiServer from './ApiServer';
import BusyIndicator from './BusyIndicator';

export default class MainLayout extends React.Component {
    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();
        this.roleMatcher = new RoleMatcher();

        this.state = {
            privilegeProfile: null
        };

        this.pickPrivilegeProfile = this.pickPrivilegeProfile.bind(this);
    }

    render() {
        return (
            <div>
                {this.state.privilegeProfile === null ?
                    <Navbar fluid>
                        <p className="navbar-text"><BusyIndicator/></p>
                    </Navbar> :
                    <Navbar fluid>
                        <Nav pullLeft>
                            { this.roleMatcher.isAdmin(this.state.privilegeProfile.roles) ? <LinkContainer to="request/priorities"><NavItem>Priorities</NavItem></LinkContainer> : null }
                            { this.roleMatcher.isAdmin(this.state.privilegeProfile.roles) ? <LinkContainer to="purchase-request/types"><NavItem>Types</NavItem></LinkContainer> : null }
                            { this.roleMatcher.isAdmin(this.state.privilegeProfile.roles) ? <LinkContainer to="purchase-request/conditions"><NavItem>Conditions</NavItem></LinkContainer> : null }
                            { this.roleMatcher.isAdmin(this.state.privilegeProfile.roles) ? <LinkContainer to="user/accounts"><NavItem>Clients</NavItem></LinkContainer> : null }
                            { this.roleMatcher.isClient(this.state.privilegeProfile.roles) && !this.roleMatcher.isAdmin(this.state.privilegeProfile.roles)? <LinkContainer to="requests-as-client"><NavItem>My requests</NavItem></LinkContainer> : null }
                            { this.roleMatcher.isVendorPurchase(this.state.privilegeProfile.roles)
                              || this.roleMatcher.isVendorRepair(this.state.privilegeProfile.roles)
                              || this.roleMatcher.isAdmin(this.state.privilegeProfile.roles)
                                ? <LinkContainer to="requests-as-vendor"><NavItem>Requests</NavItem></LinkContainer>
                                : null
                            }
                        </Nav>
                        <Nav pullRight>
                            <LinkContainer to="logout"><NavItem>Logout</NavItem></LinkContainer>
                        </Nav>
                    </Navbar>
                }
                {this.props.children}
            </div>
        );
    }

    componentDidMount() {
        this.pickPrivilegeProfile();
    }

    pickPrivilegeProfile() {
        this.apiServer.get('/privilege/pick-profile').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');

                return;
            }

            this.setState({
                privilegeProfile: res.body
            });
        }.bind(this));
    }
}

class RoleMatcher {
    isAdmin(roles) {
        return this._hasRole('admin', roles);
    }

    isClient(roles) {
        return this._hasRole('client', roles);
    }

    isVendorPurchase(roles) {
        return this._hasRole('vendor_purchase', roles);
    }

    isVendorRepair(roles) {
        return this._hasRole('vendor_repair', roles);
    }

    _hasRole(role, roles) {
        return roles.indexOf(role) > -1;
    }
}
