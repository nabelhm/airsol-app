import React from 'react';
import {render} from 'react-dom';
import {Router, Route, Link, IndexRoute} from 'react-router';
import createHashHistory from 'history/lib/createHashHistory';

class App extends React.Component {
    render() {
        return (
            this.props.children
        )
    }
}

import UserAccounts from './User/Accounts';
import RequestPriorities from './Request/Priorities';
import PurchaseRequestTypes from './PurchaseRequest/Types';
import PurchaseRequestConditions from './PurchaseRequest/Conditions';
import Dashboard from './Dashboard';
import Login from './Login';
import Logout from './Logout';
import MainLayout from './MainLayout';
import RequestsAsClient from './RequestsAsClient';
import RequestsAsVendor from './RequestsAsVendor';

render((
    <Router history={createHashHistory()}>
        <Route path="/" component={MainLayout}>
            <IndexRoute component={Dashboard}/>
            <Route path="request/priorities" component={RequestPriorities}/>
            <Route path="purchase-request/types" component={PurchaseRequestTypes}/>
            <Route path="purchase-request/conditions" component={PurchaseRequestConditions}/>
            <Route path="user/accounts" component={UserAccounts}/>
            <Route path="dashboard" component={Dashboard}/>
            <Route path="requests-as-client" component={RequestsAsClient}/>
            <Route path="requests-as-vendor(/:request/:action/:token)" component={RequestsAsVendor}/>
        </Route>
        <Route path="login" component={Login}/>
        <Route path="logout" component={Logout}/>
    </Router>
), document.getElementById('app'));