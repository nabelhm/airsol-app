import React from 'react';
import {Grid} from 'react-bootstrap';

import BusyIndicator from './BusyIndicator';

export default class Logout extends React.Component {
    constructor(props) {
        super(props);

        this.storer = require('sessionstorage');
    }
    
    componentDidMount() {
        this.storer.clear();

        this.props.history.replaceState(null, '/login');
    }
    
    render() {
        return (
            <Grid fluid><BusyIndicator/></Grid>
        );
    }
}