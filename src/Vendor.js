import React from 'react';
import {Col, Row} from 'react-bootstrap';

export default class Vendor extends React.Component {
    static propTypes: {
        vendor: React.PropTypes.object.isRequired
    };
    
    render() {
        return (
            <Row>
                <Col md={3}>
                    <p><strong>From:</strong> {this.props.vendor.name}</p>
                    <p><img src={this.props.vendor.logo} style={{'width': 80, 'height': 80}}/></p>
                </Col>
                <Col md={9}>
                    {this.props.vendor.company}<br />
                    {this.props.vendor.title == 'mr' ? 'Mr' : 'Ms'} {this.props.vendor.firstName}  {this.props.vendor.lastName}<br />
                    {this.props.vendor.address}, {this.props.vendor.zip}, {this.props.vendor.city}, {this.props.vendor.country}<br />
                    {this.props.vendor.phone}<br />
                    {this.props.vendor.email}
                </Col>
            </Row>
        );
    }
}