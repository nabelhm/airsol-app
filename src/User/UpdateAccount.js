import React from 'react';
import {SelectList} from 'react-widgets';
import {Col, Grid, Input, ButtonToolbar, Modal, Row} from 'react-bootstrap';
import {DropdownList} from 'react-widgets';

import ApiServer from '../ApiServer';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';

export default class UpdateAccount extends React.Component {
    static propTypes: {
        account: React.PropTypes.object.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            overlay: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleUpdate = this._handleUpdate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <div>
                <Button
                    icon="edit"
                    textBefore="Edit info"
                    onClick={this._handleClick}
                />
                <Modal show={this.state.overlay} backdrop={false} enforceFocus={false} onHide={this._handleCancel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit info</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form account={this.props.account} onUpdate={this._handleUpdate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

    _handleClick(finish) {
        this.setState({
            overlay: true
        });

        finish();
    }

    _handleUpdate(accounts) {
        this.setState({
            overlay: false
        });

        this.props.onUpdate(accounts);
    }

    _handleCancel() {
        this.setState({
            overlay: false
        });
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class Form extends React.Component {
    static propTypes: {
        account: React.PropTypes.object.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();

        this.state = {
            request: this.props.account,
            response: null
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleChangeTitle = this._handleChangeTitle.bind(this);
        this._handleChangeCountry = this._handleChangeCountry.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form">
                  {
                      this.state.response !== null ?
                          <ResponseAlert
                              status={this.state.response.status}
                              message={this._resolveMessage(this.state.response)}
                          />
                      :
                          null
                  }

                  <Row>
                      <Col md={6}>
                          <div className="form-group">
                              <label className="control-label col-xs-4">Title</label>
                              <div className="col-xs-8">
                                  <DropdownList
                                      ref='title'
                                      data={[{id: 'mr', name: 'Mr.'}, {id: 'ms', name: 'Ms.'}]}
                                      value={this.state.request.title}
                                      valueField='id'
                                      textField='name'
                                      onChange={this._handleChangeTitle}
                                  />
                              </div>
                          </div>
                          <Input
                              ref="firstName"
                              value={this.state.request.firstName}
                              type="text"
                              rows="4"
                              label="First name *"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="lastName"
                              value={this.state.request.lastName}
                              type="text"
                              label="Last name *"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="company"
                              value={this.state.request.company}
                              type="text"
                              label="Company *"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="phone"
                              value={this.state.request.phone}
                              type="text"
                              label="Phone *"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                      </Col>
                      <Col md={6}>
                          <Input
                              ref="address"
                              value={this.state.request.address}
                              type="textarea"
                              label="Address *"
                              rows="4"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="city"
                              value={this.state.request.city}
                              type="text"
                              label="City *"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="zip"
                              value={this.state.request.zip}
                              type="text"
                              label="Zip"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <div className="form-group">
                              <label className="control-label col-xs-4">Country *</label>
                              <div className="col-xs-8">
                                  <DropdownList
                                      ref='country'
                                      data={[{id: "AF", name: "Afghanistan"}, {id: "AL", name: "Albania"}, {id: "DZ", name: "Algeria"}, {id: "AS", name: "American Samoa"}, {id: "AD", name: "Andorra"}, {id: "AO", name: "Angola"}, {id: "AI", name: "Anguilla"}, {id: "AQ", name: "Antarctica"}, {id: "AG", name: "Antigua and Barbuda"}, {id: "AR", name: "Argentina"}, {id: "AM", name: "Armenia"}, {id: "AW", name: "Aruba"}, {id: "AU", name: "Australia"}, {id: "AT", name: "Austria"}, {id: "AZ", name: "Azerbaijan"}, {id: "BS", name: "Bahamas"}, {id: "BH", name: "Bahrain"}, {id: "BD", name: "Bangladesh"}, {id: "BB", name: "Barbados"}, {id: "BY", name: "Belarus"}, {id: "BE", name: "Belgium"}, {id: "BZ", name: "Belize"}, {id: "BJ", name: "Benin"}, {id: "BM", name: "Bermuda"}, {id: "BT", name: "Bhutan"}, {id: "BO", name: "Bolivia"}, {id: "BA", name: "Bosnia and Herzegovina"}, {id: "BW", name: "Botswana"}, {id: "BR", name: "Brazil"}, {id: "BN", name: "Brunei Darussalam"}, {id: "BG", name: "Bulgaria"}, {id: "BF", name: "Burkina Faso"}, {id: "BI", name: "Burundi"}, {id: "KH", name: "Cambodia"}, {id: "CM", name: "Cameroon"}, {id: "CA", name: "Canada"}, {id: "CV", name: "Cape Verde"}, {id: "KY", name: "Cayman Islands"}, {id: "CF", name: "Central African Republic"}, {id: "TD", name: "Chad"}, {id: "CL", name: "Chile"}, {id: "CN", name: "China"}, {id: "CX", name: "Christmas Island"}, {id: "CC", name: "Cocos (Keeling) Island"}, {id: "CO", name: "Colombia"}, {id: "KM", name: "Comoro Islands"}, {id: "CG", name: "Congo"}, {id: "CD", name: "Congo, The Democratic Republic of the"}, {id: "CK", name: "Cook Islands"}, {id: "CR", name: "Costa Rica"}, {id: "CI", name: "Cote d'Ivoire"}, {id: "HR", name: "Croatia"}, {id: "CW", name: "Curacao"}, {id: "CY", name: "Cyprus"}, {id: "CZ", name: "Czech Republic"}, {id: "DK", name: "Denmark"}, {id: "DJ", name: "Djibouti"}, {id: "DM", name: "Dominica"}, {id: "DO", name: "Dominican Republic"}, {id: "EC", name: "Ecuador"}, {id: "EG", name: "Egypt"}, {id: "SV", name: "El Salvador"}, {id: "GQ", name: "Equatorial Guinea"}, {id: "ER", name: "Eritrea"}, {id: "EE", name: "Estonia"}, {id: "ET", name: "Ethiopia"}, {id: "FK", name: "Falkland Islands (Malvinas)"}, {id: "FO", name: "Faroe Islands"}, {id: "FJ", name: "Fiji"}, {id: "FI", name: "Finland"}, {id: "FR", name: "France"}, {id: "GF", name: "French Guiana"}, {id: "PF", name: "French Polynesia"}, {id: "GA", name: "Gabon"}, {id: "GM", name: "Gambia"}, {id: "GE", name: "Georgia"}, {id: "DE", name: "Germany"}, {id: "GH", name: "Ghana"}, {id: "GI", name: "Gibraltar"}, {id: "GR", name: "Greece"}, {id: "GL", name: "Greenland"}, {id: "GD", name: "Grenada"}, {id: "GP", name: "Guadeloupe"}, {id: "GU", name: "Guam"}, {id: "GT", name: "Guatemala"}, {id: "GN", name: "Guinea"}, {id: "GW", name: "Guinea-Bissau"}, {id: "GY", name: "Guyana"}, {id: "HT", name: "Haiti"}, {id: "VA", name: "Holy See (Vatican City State)"}, {id: "HN", name: "Honduras"}, {id: "HK", name: "Hong Kong"}, {id: "HU", name: "Hungary"}, {id: "IS", name: "Iceland"}, {id: "IN", name: "India"}, {id: "ID", name: "Indonesia"}, {id: "IQ", name: "Iraq"}, {id: "IE", name: "Ireland"}, {id: "IL", name: "Israel"}, {id: "IT", name: "Italy"}, {id: "JM", name: "Jamaica"}, {id: "JP", name: "Japan"}, {id: "JO", name: "Jordan"}, {id: "KZ", name: "Kazakstan"}, {id: "KE", name: "Kenya"}, {id: "KI", name: "Kiribati"}, {id: "KR", name: "Korea, Republic of"}, {id: "KW", name: "Kuwait"}, {id: "KG", name: "Kyrgyzstan"}, {id: "LA", name: "Lao People's Democratic Republic"}, {id: "LV", name: "Latvia"}, {id: "LB", name: "Lebanon"}, {id: "LS", name: "Lesotho"}, {id: "LR", name: "Liberia"}, {id: "LY", name: "Libyan Arab Jamahiriya"}, {id: "LI", name: "Liechtenstein"}, {id: "LT", name: "Lithuania"}, {id: "LU", name: "Luxembourg"}, {id: "MO", name: "Macau"}, {id: "MK", name: "Macedonia, The Former Yugoslav Republic of"}, {id: "MG", name: "Madagascar"}, {id: "MW", name: "Malawi"}, {id: "MY", name: "Malaysia"}, {id: "MV", name: "Maldives"}, {id: "ML", name: "Mali"}, {id: "MT", name: "Malta"}, {id: "MH", name: "Marshall Islands"}, {id: "MQ", name: "Martinique"}, {id: "MR", name: "Mauritania"}, {id: "MU", name: "Mauritius"}, {id: "YT", name: "Mayotte"}, {id: "MX", name: "Mexico"}, {id: "FM", name: "Micronesia, Federated States of"}, {id: "MD", name: "Moldova, Republic of"}, {id: "MC", name: "Monaco"}, {id: "MN", name: "Mongolia"}, {id: "ME", name: "Montenegro"}, {id: "MS", name: "Montserrat"}, {id: "MA", name: "Morocco"}, {id: "MZ", name: "Mozambique"}, {id: "NA", name: "Namibia"}, {id: "NR", name: "Nauru"}, {id: "NP", name: "Nepal"}, {id: "NL", name: "Netherlands"}, {id: "AN", name: "Netherlands Antilles"}, {id: "NC", name: "New Caledonia"}, {id: "NZ", name: "New Zealand"}, {id: "NI", name: "Nicaragua"}, {id: "NE", name: "Niger"}, {id: "NG", name: "Nigeria"}, {id: "NU", name: "Niue"}, {id: "NF", name: "Norfolk Island"}, {id: "MP", name: "Northern Mariana Islands"}, {id: "NO", name: "Norway"}, {id: "OM", name: "Oman"}, {id: "PK", name: "Pakistan"}, {id: "PW", name: "Palau"}, {id: "PS", name: "Palestinian Territory,Occupied"}, {id: "PA", name: "Panama"}, {id: "PG", name: "Papua New Guinea"}, {id: "PY", name: "Paraguay"}, {id: "PE", name: "Peru"}, {id: "PH", name: "Philippines"}, {id: "PL", name: "Poland"}, {id: "PT", name: "Portugal"}, {id: "PR", name: "Puerto Rico"}, {id: "QA", name: "Qatar"}, {id: "RE", name: "Reunion"}, {id: "RO", name: "Romania"}, {id: "RU", name: "Russian Federation"}, {id: "RW", name: "Rwanda"}, {id: "SH", name: "Saint Helena"}, {id: "KN", name: "Saint Kitts and Nevi"}, {id: "LC", name: "Saint Lucia"}, {id: "PM", name: "Saint Pierre and Miquelon"}, {id: "VC", name: "Saint Vincent and the Grenadines"}, {id: "WS", name: "Samoa"}, {id: "SM", name: "San Marino"}, {id: "ST", name: "Sao Tome and Principe"}, {id: "SA", name: "Saudi Arabia"}, {id: "SN", name: "Senegal"}, {id: "RS", name: "Serbia"}, {id: "CS", name: "Serbia and Montenegro"}, {id: "SC", name: "Seychelles"}, {id: "SL", name: "Sierra Leone"}, {id: "SG", name: "Singapore"}, {id: "SK", name: "Slovakia"}, {id: "SI", name: "Slovenia"}, {id: "SB", name: "Solomon Islands"}, {id: "SO", name: "Somalia"}, {id: "ZA", name: "South Africa"}, {id: "ES", name: "Spain"}, {id: "LK", name: "Sri Lanka"}, {id: "SR", name: "Suriname"}, {id: "SZ", name: "Swaziland"}, {id: "SE", name: "Sweden"}, {id: "CH", name: "Switzerland"}, {id: "TW", name: "Taiwan"}, {id: "TJ", name: "Tajikistan"}, {id: "TZ", name: "Tanzania, United Republic of"}, {id: "TH", name: "Thailand"}, {id: "TL", name: "Timor-Leste"}, {id: "TG", name: "Togo"}, {id: "TK", name: "Tokelau"}, {id: "TO", name: "Tonga"}, {id: "TT", name: "Trinidad and Tobago"}, {id: "TN", name: "Tunisia"}, {id: "TR", name: "Turkey"}, {id: "TM", name: "Turkmenistan"}, {id: "TC", name: "Turks and Caicos Islands"}, {id: "TV", name: "Tuvalu"}, {id: "UG", name: "Uganda"}, {id: "UA", name: "Ukraine"}, {id: "AE", name: "United Arab Emirates"}, {id: "GB", name: "United Kingdom"}, {id: "US", name: "United States"}, {id: "UY", name: "Uruguay"}, {id: "UZ", name: "Uzbekistan"}, {id: "VU", name: "Vanuatu"}, {id: "VE", name: "Venezuela"}, {id: "VN", name: "Viet Nam"}, {id: "VG", name: "Virgin Islands, British"}, {id: "VI", name: "Virgin Islands, U.S."}, {id: "WF", name: "Wallis and Futuna"}, {id: "YE", name: "Yemen"}, {id: "YU", name: "Yugoslavia"}, {id: "ZM", name: "Zambia"}]}
                                      defaultValue={"US"}
                                      value={this.state.request.country}
                                      valueField='id'
                                      textField='name'
                                      onChange={this._handleChangeCountry}/>
                              </div>
                          </div>
                          <Input
                              ref="website"
                              value={this.state.request.website}
                              type="text"
                              label="Website"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                          <Input
                              ref="comments"
                              value={this.state.request.comments}
                              type="textarea"
                              label="Comments"
                              labelClassName="col-xs-4"
                              wrapperClassName="col-xs-8"
                              onChange={this._handleChange}/>
                      </Col>
                  </Row>

                  <div className="form-group">
                      <div className="col-sm-offset-4 col-sm-8">
                          <ButtonToolbar>
                              <Button
                                  type="submit"
                                  icon="ok-sign"
                                  textBefore="Update account"
                                  textAfter="Updating account..."
                                  bsStyle="primary"
                                  onClick={this._handleSubmit}/>
                              <Button
                                  icon="remove-sign"
                                  textBefore="Cancel"
                                  onClick={this._handleCancel}/>
                          </ButtonToolbar>
                      </div>
                  </div>
            </form>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                ...this.state.request,
                firstName: this.refs.firstName.getValue(),
                lastName: this.refs.lastName.getValue(),
                company: this.refs.company.getValue(),
                city: this.refs.city.getValue(),
                address: this.refs.address.getValue(),
                zip: this.refs.zip.getValue(),
                website: this.refs.website.getValue(),
                phone: this.refs.phone.getValue(),
                comments: this.refs.comments.getValue()
            }
        });
    }

    _handleChangeTitle(title) {
        this.setState({
            request: {
                ...this.state.request,
                title: title.id
            }
        });
    }

    _handleChangeCountry(country) {
        this.setState({
            request: {
                ...this.state.request,
                country: country.id
            }
        });
    }

    _handleSubmit(finish) {
        this.setState({
            response: null
        });

        this.apiServer
            .post('/user/update-account/' + this.props.account.uniqueness)
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onUpdate(res.body);
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }

    _resolveMessage(response) {
        let message = "";

        switch(response.status) {
            case 400:
                switch(response.body.code) {
                    case 'USER.ACCOUNT.REQUIRED_FIELD':

                        message = 'The ' + response.body.field + ' is required.';

                        break;
                }

                break;
        }

        return message;
    }
}
