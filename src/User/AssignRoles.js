import React from 'react';
import {SelectList} from 'react-widgets';
import {Grid, Input, ButtonToolbar, Modal} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';

export default class AssignRoles extends React.Component {
    static propTypes: {
        account: React.PropTypes.object.isRequired,
        roles: React.PropTypes.array.isRequired,
        onAssign: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            overlay: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleAssign = this._handleAssign.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <div>
                <Button
                    icon="edit"
                    textBefore="Assign roles"
                    onClick={this._handleClick}
                />
                <Modal show={this.state.overlay} backdrop={false} enforceFocus={false} onHide={this._handleCancel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Assign roles</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form account={this.props.account} roles={this.props.roles} onAssign={this._handleAssign} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

    _handleClick(finish) {
        this.setState({
            overlay: true
        });

        finish();
    }

    _handleAssign(accounts) {
        this.setState({
            overlay: false
        });

        this.props.onAssign(accounts);
    }
    
    _handleCancel() {
        this.setState({
            overlay: false
        });
    }
    
    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class Form extends React.Component {
    static propTypes: {
        account: React.PropTypes.object.isRequired,
        roles: React.PropTypes.array.isRequired,
        onAssign: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            request: {
                body: this.props.account.body,
                roles: this.props.account.roles
            },
            response: {
                status: null,
                body: null
            }
        };

        this.apiServer = new ApiServer();

        this._handleChangeRoles = this._handleChangeRoles.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form" onSubmit={this._handleSubmit}>
                <div className="form-group">
                    <label className="control-label col-xs-2">Roles</label>
                    <div className="col-xs-10">
                        <SelectList
                            ref='roles'
                            value={this.state.request.roles}
                            data={this.props.roles}
                            valueField='code'
                            textField='title'
                            multiple
                            onChange={this._handleChangeRoles}/>
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <ButtonToolbar>
                            <Button
                                type="submit"
                                icon="ok-sign"
                                textBefore="Assign"
                                textAfter="Asignining..."
                                bsStyle="primary"
                                onClick={this._handleSubmit}
                            />
                            <Button
                                icon="remove-sign"
                                textBefore="Cancel"
                                onClick={this._handleCancel}
                            />
                        </ButtonToolbar>
                    </div>
                </div>
            </form>
        );
    }
    
    _handleChangeRoles(roles) {
        Object.keys(roles).map(function (index) {
            roles[index] = roles[index].code;
        });

        this.setState({
            request: {
                body: this.state.request.body,
                roles: roles
            }
        });
    }
    
    _handleSubmit(finish) {
        this.setState({
            response: {
                status: null,
                body: null
            }
        });

        this.apiServer
            .post('/privilege/update-profile/' + this.props.account.uniqueness)
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onAssign(res.body);
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }
    
    _handleCancel(finish) {
        finish();

        this.props.onCancel();
    }
}