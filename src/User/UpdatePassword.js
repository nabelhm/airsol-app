import React from 'react';
import {SelectList} from 'react-widgets';
import {Col, Grid, Input, ButtonToolbar, Modal, Row} from 'react-bootstrap';
import {DropdownList} from 'react-widgets';

import ApiServer from '../ApiServer';
import Button from '../Button';
import ResponseAlert from '../ResponseAlert';

export default class UpdatePassword extends React.Component {
    static propTypes: {
        uniqueness: React.PropTypes.string.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            overlay: false
        };

        this._handleClick = this._handleClick.bind(this);
        this._handleUpdate = this._handleUpdate.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
        this._handleUnauthorized = this._handleUnauthorized.bind(this);
    }

    render() {
        return (
            <div>
                <Button
                    icon="edit"
                    textBefore="Edit password"
                    onClick={this._handleClick}
                />
                <Modal show={this.state.overlay} backdrop={false} enforceFocus={false} onHide={this._handleCancel}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form uniqueness={this.props.uniqueness} onUpdate={this._handleUpdate} onCancel={this._handleCancel} onUnauthorized={this._handleUnauthorized}/>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }

    _handleClick(finish) {
        this.setState({
            overlay: true
        });

        finish();
    }

    _handleUpdate(accounts) {
        this.setState({
            overlay: false
        });

        this.props.onUpdate(accounts);
    }

    _handleCancel() {
        this.setState({
            overlay: false
        });
    }

    _handleUnauthorized() {
        this.props.onUnauthorized();
    }
}

class Form extends React.Component {
    static propTypes: {
        uniqueness: React.PropTypes.uniqueness.isRequired,
        onUpdate: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();

        this.state = {
            request: this.props.account,
            response: null
        };

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
        this._handleCancel = this._handleCancel.bind(this);
    }

    render() {
        return (
            <form className="form-horizontal" role="form">
                  <Row>
                    <Col md={12}>
                      <Input
                        ref="password"
                        type="text"
                        rows="4"
                        label="New password"
                        labelClassName="col-xs-2"
                        wrapperClassName="col-xs-4"
                        onChange={this._handleChange}/>
                    </Col>
                  </Row>

                  <div className="form-group">
                      <div className="col-sm-offset-2 col-sm-6">
                          <ButtonToolbar>
                              <Button
                                  type="submit"
                                  icon="ok-sign"
                                  textBefore="Update password"
                                  textAfter="Updating password..."
                                  bsStyle="primary"
                                  onClick={this._handleSubmit}/>
                              <Button
                                  icon="remove-sign"
                                  textBefore="Cancel"
                                  onClick={this._handleCancel}/>
                          </ButtonToolbar>
                      </div>
                  </div>
            </form>
        );
    }

    _handleChange() {
        this.setState({
            request: {
                ...this.state.request,
                password: this.refs.password.getValue(),
            }
        });
    }

    _handleSubmit(finish) {
        this.setState({
            response: null
        });

        this.apiServer
            .post('/user/update-password/' + this.props.uniqueness)
            .auth()
            .send(this.state.request)
            .end(function (err, res) {
                finish();

                if (!err) {
                    this.props.onUpdate(res.body);
                } else {
                    if (err.status === 401) {
                        this.props.onUnauthorized();
                    }

                    this.setState({
                        response: {
                            status: res.status,
                            body: res.body
                        }
                    });
                }
            }.bind(this));
    }

    _handleCancel() {
        this.props.onCancel();
    }
}
