import React from 'react';

import ApiServer from '../ApiServer';
import Button from '../Button';

export default class DeleteAccount extends React.Component {
    static propTypes: {
        uniqueness: React.PropTypes.string.isRequired,
        onDelete: React.PropTypes.func.isRequired,
        onUnauthorized: React.PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.apiServer = new ApiServer();

        this._handleClick = this._handleClick.bind(this);
    }

    render() {
        return (
            <Button
                icon="remove"
                textBefore="Delete"
                textAfter="Deleting..."
                onClick={this._handleClick}
            />
        );
    }

    _handleClick(finish) {
        this.apiServer.get('/user/delete-account/' + this.props.uniqueness).auth().end(function (err, res) {
            finish();

            if (err && err.status === 401) {
                this.props.onUnauthorized();
            }

            this.props.onDelete(res.body);
        }.bind(this));
    }
}