import React from 'react';
import {Col, ButtonToolbar, Grid, Input, Modal, Panel, Table, Row, Well} from 'react-bootstrap';

import ApiServer from '../ApiServer';
import BusyIndicator from '../BusyIndicator';
import AssignRoles from './AssignRoles';
import DeleteAccount from './DeleteAccount';
import UpdateAccount from './UpdateAccount';
import UpdatePassword from './UpdatePassword';

export default class Accounts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            roles: null,
            accounts: null
        };

        this.apiServer = new ApiServer();

        this._collectRoles = this._collectRoles.bind(this);
        this._collectAccounts = this._collectAccounts.bind(this);
        this._handleChange = this._handleChange.bind(this);
        this._buildRenderedUnassignedAccounts = this._buildRenderedUnassignedAccounts.bind(this);
        this._buildRenderedAccounts = this._buildRenderedAccounts.bind(this);
        this._resolveRoleTitle = this._resolveRoleTitle.bind(this);
    }

    render() {
        if (this.state.accounts !== null && this.state.roles) {
            var renderedUnassignedAccounts = this._buildRenderedUnassignedAccounts(this.state.accounts, this.state.roles);

            var unassignedAccountsPanel = (renderedUnassignedAccounts.length > 0
                ?
                    <Panel header="List of new client">
                        <Row>
                            <Col md={3}><h4>Name</h4></Col>
                            <Col md={2}><h4>Company</h4></Col>
                            <Col md={7}></Col>
                        </Row>
                        {renderedUnassignedAccounts}
                    </Panel>
                :
                    null
            );

            var renderedAccounts = this._buildRenderedAccounts(this.state.accounts, this.state.roles);

            var accountsTable = (renderedAccounts.length == 0
                ?
                    'No users'
                :
                    <div>
                        <Row>
                            <Col md={3}><h4>Name</h4></Col>
                            <Col md={2}><h4>Company</h4></Col>
                            <Col md={3}><h4>Roles</h4></Col>
                            <Col md={4}></Col>
                        </Row>
                        {renderedAccounts}
                    </div>
            );
        }

        return (
            <Grid fluid>
                {unassignedAccountsPanel}
                <Panel header="List of client">
                    { this.state.accounts === null || this.state.roles === null ? <BusyIndicator/> : accountsTable}
                </Panel>
            </Grid>
        )
    }

    componentDidMount() {
        this._collectRoles();
        this._collectAccounts();
    }

    _collectRoles() {
        this.apiServer.get('/privilege/collect-roles').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');
            }

            this.setState({
                roles: res.body
            });
        }.bind(this));
    }

    _collectAccounts() {
        this.apiServer.get('/user/collect-accounts').auth().end(function (err, res) {
            if (err && err.status === 401) {
                this.props.history.replaceState(null, '/login');
            }

            this.setState({
                accounts: res.body
            });
        }.bind(this));
    }

    _handleChange(accounts) {
        this.setState({
            accounts: accounts
        });
    }

    _buildRenderedUnassignedAccounts(accounts, roles) {
        var renderedUnassignedAccounts = [];

        accounts.forEach(function (account) {
            if (account['roles'].length == 0) {
                renderedUnassignedAccounts.push(
                    <Row key={account.uniqueness} style={{marginBottom: "10px"}}>
                        <Col md={3}>
                            {account.firstName} {account.lastName}
                        </Col>
                        <Col md={2}>
                            {account.company}
                        </Col>
                        <Col md={7}>
                            <ButtonToolbar>
                                <AssignRoles account={account} roles={roles} onAssign={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                            </ButtonToolbar>
                        </Col>
                    </Row>
                );
            }
        }.bind(this));

        return renderedUnassignedAccounts;
    }

    _buildRenderedAccounts(accounts, roles) {
        var renderedAccounts = [];

        accounts.forEach(function(account) {
            if (account.roles.length > 0) {
                var roleTitles = account.roles.map(function(role) {
                    return (
                        <li key={role}>{this._resolveRoleTitle(role, roles)}</li>
                    );
                }.bind(this));

                renderedAccounts.push(
                  <Row key={account.uniqueness} style={{marginBottom: "10px"}}>
                      <Col md={2}>
                          {account.firstName} {account.lastName}
                      </Col>
                      <Col md={2}>
                          {account.company}
                      </Col>
                      <Col md={3}>
                          {roleTitles ? <ul className="list-unstyled">{roleTitles}</ul> : null}
                      </Col>
                      <Col md={5}>
                          <ButtonToolbar>
                              <UpdateAccount account={account} onUpdate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                              <AssignRoles account={account} roles={roles} onAssign={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                              <UpdatePassword uniqueness={account.uniqueness} onUpdate={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                              <DeleteAccount uniqueness={account.uniqueness} onDelete={this._handleChange} onUnauthorized={this._handleUnauthorized}/>
                          </ButtonToolbar>
                      </Col>
                  </Row>
                );
            }
        }.bind(this));

        return renderedAccounts;
    }

    _resolveRoleTitle(code, roles) {
        let role = roles.find(function(role) {
            return code == role.code;
        });

        return role.title;
    }
}
